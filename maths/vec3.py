# vec3 :
# [x: float, y: float, z: float]
#
# Represente un vecteur en 3 dimensions qui sert a representer un point ou un vecteur selon la situation

from math import cos, sin, acos, pi
import random as rand


# Retourne le negatif du vec3
def neg(v):
    return [
        -v[0],
        -v[1],
        -v[2]
    ]

# Retourne la somme entre 2 vec3
def add(v, v1):
    return [
        v[0] + v1[0],
        v[1] + v1[1],
        v[2] + v1[2]
    ]
# Retourne la somme entre un vec3 et un float
def add_n(v, n):
    return [
        v[0] + n,
        v[1] + n,
        v[2] + n
    ]
# Additionne v1 a v
def addeq(v, v1):
    v[0] += v1[0]
    v[1] += v1[1]
    v[2] += v1[2]
# Additionne n a v
def addeq_n(v, n):
    v[0] += n
    v[1] += n
    v[2] += n

# Retourne la difference entre 2 vec3
def sub(v, v1):
    return [
        v[0] - v1[0],
        v[1] - v1[1],
        v[2] - v1[2]
    ]
# Retourne la difference entre un vec3 et un float
def sub_n(v, n):
    return [
        v[0] - n,
        v[1] - n,
        v[2] - n
    ]
# Soustrait v1 a v
def subeq(v, v1):
    v[0] -= v1[0]
    v[1] -= v1[1]
    v[2] -= v1[2]
# Soustrait n a v
def subeq_n(v, n):
    v[0] -= n
    v[1] -= n
    v[2] -= n

# Retourne le produit entre 2 vec3
def mul(v, v1):
    return [
        v[0] * v1[0],
        v[1] * v1[1],
        v[2] * v1[2]
    ]
# Retourne le produit entre un vec3 et un float
def mul_n(v, n):
    return [
        v[0] * n,
        v[1] * n,
        v[2] * n
    ]
# Multiplie v par v1
def muleq(v, v1):
    v[0] *= v1[0]
    v[1] *= v1[1]
    v[2] *= v1[2]
# Multiplie v par n
def muleq_n(v, n):
    v[0] *= n
    v[1] *= n
    v[2] *= n

# Retourne le quotient entre 2 vec3
def div(v, v1):
    return [
        v[0] / v1[0],
        v[1] / v1[1],
        v[2] / v1[2]
    ]
# Retourne le quotient entre un vec3 et un float
def div_n(v, n):
    return [
        v[0] / n,
        v[1] / n,
        v[2] / n
    ]
# Divise v par v1
def diveq(v, v1):
    v[0] /= v1[0]
    v[1] /= v1[1]
    v[2] /= v1[2]
# Divise v par n
def diveq_n(v, n):
    v[0] /= n
    v[1] /= n
    v[2] /= n


# Retourne le produit vectoriel de v par v1
def cross(v, v1):
    return [
        (v[1] * v1[2]) - (v[2] * v1[1]),
        (v[2] * v1[0]) - (v[0] * v1[2]),
        (v[0] * v1[1]) - (v[1] * v1[0])
    ]

# Retourne scalaire de v par v1
def dot(v, v1):
    return (v[0] * v1[0] +
            v[1] * v1[1] +
            v[2] * v1[2])

from math import sqrt
# Retourne la norme de v
def length(v):
    return sqrt(length_sq(v))

# Retourne le carre de la norme de v
def length_sq(v):
    return (v[0] * v[0] +
            v[1] * v[1] +
            v[2] * v[2])

# Retourne le vecteur unitaire de v
def normalize(v):
    return div_n(v, length(v))

# Set le vecteur unitaire de v
def normalize_eq(v):
    return diveq_n(v, length(v))

# Retourne une representation de v en str
def to_str(v):
    return "vec3 { %f, %f, %f }" % (v[0], v[1], v[2])


def rotx(v, a):
    c = cos(a)
    s = sin(a)
    return [
        v[0],
        c * v[1] - s * v[2],
        s * v[1] + c * v[2]
    ]

def roty(v, a):
    c = cos(a)
    s = sin(a)
    return [
        s * v[2] + c * v[0],
        v[1],
        c * v[2] - s * v[0]
    ]

def rotz(v, a):
    c = cos(a)
    s = sin(a)
    return [
        c * v[0] - s * v[1],
        s * v[0] + c * v[1],
        v[2]
    ]

def random():
    phi = rand.uniform(0, pi*2)
    theta = acos(rand.uniform(-1, 1))

    x = sin(theta) * cos(phi)
    y = sin(theta) * sin(phi)
    z = cos(theta)

    return (x,y,z)

def random_unit():
    return normalize(random())