from render import hittable, texture


def two_balls_on_a_big_ball():
    return (
        hittable.LIST,
        (
            (
                hittable.SPHERE,
                (
                    (0.5, 0, 2), 0.5,
                    (
                        True,
                        0,
                        True,
                        1.5,
                        False,
                        (
                            texture.COLOR,
                            (1, 1, 1)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (-0.5, 0, 2), 0.5,
                    (
                        True,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.COLOR,
                            (0.8, 0.8, 0.8)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (0, -100.5, 2), 100,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.CHECKER_3D,
                            (
                                (
                                    texture.COLOR,
                                    (0, 1, 0)
                                ),
                                (
                                    texture.COLOR,
                                    (0, 0, 1)
                                )
                            )
                        )
                    )
                )
            )
        )
    ), (
        ((0, 10, 2), (10, 10, 10)),
        ((5, 10, 7), (10, 10, 10)),
        ((-5, 10, -3), (10, 10, 10)),
        ((5, 10, -3), (10, 10, 10)),
        ((-5, 10, 7), (10, 10, 10)),
    ), (0.7, 0.7, 1)


def two_balls_on_a_big_ball_hollow_ball():
    return (
        hittable.LIST,
        (
            (
                hittable.SPHERE,
                (
                    (0.5, 0, 2), 0.5,
                    (
                        True,
                        0,
                        True,
                        1.5,
                        False,
                        (
                            texture.COLOR,
                            (1, 1, 1)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (0.5, 0, 2), -0.49,
                    (
                        True,
                        0,
                        True,
                        1.5,
                        False,
                        (
                            texture.COLOR,
                            (1, 1, 1)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (-0.5, 0, 2), 0.5,
                    (
                        True,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.COLOR,
                            (0.8, 0.8, 0.8)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (0, -100.5, 2), 100,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.CHECKER_3D,
                            (
                                (
                                    texture.COLOR,
                                    (0, 1, 0)
                                ),
                                (
                                    texture.COLOR,
                                    (0, 0, 1)
                                )
                            )
                        )
                    )
                )
            )
        )
    ), (
        ((0, 10, 2), (10, 10, 10)),
        ((5, 10, 7), (10, 10, 10)),
        ((-5, 10, -3), (10, 10, 10)),
        ((5, 10, -3), (10, 10, 10)),
        ((-5, 10, 7), (10, 10, 10)),
    ), (0.7, 0.7, 1)


def three_balls_on_a_big_ball_light():
    return (
        hittable.LIST,
        (
            (
                hittable.SPHERE,
                (
                    (1, 0, 2), 0.5,
                    (
                        True,
                        0,
                        True,
                        1.5,
                        False,
                        (
                            texture.COLOR,
                            (1, 1, 1)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (0, 0, 2), 0.5,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.COLOR,
                            (0.5, 0.1, 0.7)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (-1, 0, 2), 0.5,
                    (
                        True,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.COLOR,
                            (0.9, 0.7, 0.8)
                        )
                    )
                )
            ),
            (
                hittable.SPHERE,
                (
                    (0, -100.5, 2), 100,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.CHECKER_3D,
                            (
                                (
                                    texture.COLOR,
                                    (0.2, 1, 0.2)
                                ),
                                (
                                    texture.COLOR,
                                    (0.2, 0.2, 1)
                                )
                            )
                        )
                    )
                )
            ),
        )
    ), (
        ((0, 10, 2), (10, 10, 10)),
        ((5, 10, 7), (10, 10, 10)),
        ((-5, 10, -3), (10, 10, 10)),
        ((5, 10, -3), (10, 10, 10)),
        ((-5, 10, 7), (10, 10, 10)),
    ), (0.7, 0.7, 1)

def cornell_box():
    return (
        hittable.LIST,
        (
            (
                hittable.TRIANGLE,
                (
                    (-10, -10, 30),
                    (-10, 10, 30),
                    (10, 10, 30),
                    (0, 0, -1),
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.COLOR,
                            (1, 0, 0)
                        )
                    )
                )
            ),
            (
                hittable.RECT,
                (
                    0,
                    (-10, 10),
                    (10, 30),
                    10,
                    1,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.LINES,
                            (
                                (
                                    texture.COLOR,
                                    (0, 1, 0),
                                ),
                                0.05,
                                (
                                    texture.COLOR,
                                    (1, 1, 0),
                                ),
                                0.05
                            )
                        )
                    )
                )
            ),
            (
                hittable.PLANE,
                (
                    1,
                    -10,
                    1,
                    (
                        False,
                        0,
                        False,
                        0,
                        False,
                        (
                            texture.CHECKER,
                            (
                                (
                                    texture.COLOR,
                                    (0.2, 0.2, 1)
                                ),
                                (
                                    texture.COLOR,
                                    (0.7, 0.7, 0.2)
                                ),
                                0.5,
                                0.5
                            )
                        )
                    )
                )
            ),
            (
                hittable.RECT,
                (
                    1,
                    (15, -5),
                    (25, 5),
                    10,
                    -1,
                    (
                        False,
                        0,
                        False,
                        0,
                        True,
                        (
                            texture.COLOR,
                            (5, 5, 5)
                        )
                    )
                )
            ),
            (
                    hittable.SPHERE,
                    (
                        (2, -4, 15), 3,
                        (
                            True,
                            0,
                            True,
                            1.5,
                            False,
                            (
                                texture.COLOR,
                                (1, 1, 1)
                            )
                        )
                    )
                ),
                (
                    hittable.BOX,
                    (
                        (-15, -7, 15),
                        (-5, -2, 22),
                        False,
                        *((
                            (
                                False,
                                0,
                                False,
                                0,
                                False,
                                (
                                    texture.COLOR,
                                    (0, 0, 1)
                                )
                            ),
                        ) * 6)
                    )
                )
            )
        ), (
            (
                (-5, 9.9999, 20),
                (2, 2, 2)
            ),
            (
                (0, 9.9999, 15),
                (2, 2, 2)
            ),
            (
                (0, 9.9999, 25),
                (2, 2, 2)
            ),
            (
                (5, 9.9999, 20),
                (2, 2, 2)
            ),
            (
                (-5, 9.9999, 15),
                (2, 2, 2)
            ),
            (
                (-5, 9.9999, 25),
                (2, 2, 2)
            ),
            (
                (5, 9.9999, 25),
                (2, 2, 2)
            ),
            (
                (5, 9.9999, 15),
                (2, 2, 2)
            ),
            (
                (0, 9.9999, 20),
                (2, 2, 2)
            ),
        ), (0, 0, 0)
