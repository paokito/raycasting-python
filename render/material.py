# material :
# [ reflects: bool, mattness: float, transparent: bool, refractive_index: float, emits: bool, tex: texture ]
#
# Ce papier m'a beaucoup aide a comprendre comment les reflections et refraxions marchent : https://graphics.stanford.edu/courses/cs148-10-summer/docs/2006--degreve--reflection_refraction.pdf

# scatter_record:
# [ r: ray, attenuation: vec3 ]

from math import sqrt
from random import random
from maths import vec3
from render import ray, texture


# Réfléchit un rayon incident sur une surface
def reflect_ray(incident, normal):
    return vec3.sub(incident, vec3.mul_n(normal, 2 * vec3.dot(incident, normal)))

# Réfracte un rayon incident dans un volume
def refract_ray(incident, normal, n):
    cosI = -vec3.dot(normal, incident)
    sinT2 = n * n * (1 - cosI * cosI)
    if sinT2 > 1: return None
    cosT = sqrt(1 - sinT2)
    return  vec3.add(vec3.mul_n(incident, n), vec3.mul_n(normal, n * cosI - cosT))

# Retourne la réflectance d'une surface
def get_reflectance(incident, normal, n):
    r0 = (1-n) / (1+n)
    r0 *= r0
    cosX = -vec3.dot(normal, incident)
    if n > 1:
        sinT2 = n * n * (1 - cosX * cosX)
        if sinT2 > 1: return 1
        cosX = sqrt(1 - sinT2)
    x = 1 - cosX
    return r0 + (1 - r0) * x * x * x * x * x


# Retourne les rayons résultants de l'intersection (rayons réfléchi et réfracté)
def scatter(material, r, hit_record):
    if material[4]: return None

    color = texture.value(material[5], hit_record[3], hit_record[4], hit_record[0]) # Couleur au point d'intersection

    if not material[0]:
        return (None, color),
    
    matte = vec3.mul_n(vec3.random(), material[1]) if material[1] else None         # Vecteur aléatoire si la surface est matte

    scatter_rec = ()

    reflectance = 1
    if material[2]:                                                                 # réfracte la lumière
        n = 1/material[3] if hit_record[5] else material[3]
        reflectance = get_reflectance(r[1], hit_record[2], n)                       # Réflectance

        if reflectance != 1:
            refract_dir = refract_ray(r[1], hit_record[2], n)
            if matte is not None:
                vec3.addeq(refract_dir, matte)
                vec3.normalize_eq()

            scatter_rec += ((ray.from_intersect_point_dir(hit_record[0], refract_dir), vec3.mul_n(color, 1-reflectance)),) # Rayon réflacté

    if reflectance != 0:
        reflect_dir = reflect_ray(r[1], hit_record[2])
        if matte is not None:
            vec3.addeq(reflect_dir, matte)
            vec3.normalize_eq()

        scatter_rec += ((ray.from_intersect_point_dir(hit_record[0], reflect_dir), vec3.mul_n(color, reflectance)),) # Rayon réfléchi

    return scatter_rec
    

# Retourne la couleur émise par la surface
def emitted(material, hit_record):
    return texture.value(material[5], hit_record[3], hit_record[4], hit_record[0]) if material[4] else [0, 0, 0]

# Retourne la transparence de la surface si elle est transparente, sinon None
def transparency(material, hit_record):
    if material[2]:
        return texture.value(material[5], hit_record[3], hit_record[4], hit_record[0])
    return None
