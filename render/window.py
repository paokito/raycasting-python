try:
    import tkinter as tk
except ImportError:
    import Tkinter as tk
import PIL.Image, PIL.ImageTk
import multiprocessing

RESOLUTION_MIN_WIDTH = 100
RESOLUTION_MAX_WIDTH = 1000
RESOLUTION_MIN_HEIGHT = 100
RESOLUTION_MAX_HEIGHT = 1000

# les "global" sont utilisés pour accéder à des variables depuis toutes
# les fonctions sans les faire passer en arguments, ce qui simplifie le code


# génère l'image
# éxécuté par le bouton "render"
def render():
    # paramètres utilisés pour afficher le résultat
    # settings = [scale_width, scale_height, ...]
    global settings
    w,h = settings[0].get(), settings[1].get()
    thread = multiprocessing.Process(target=render_function, args=(last_draw[0], w, h, int(settings[5].get())))
    thread.start()


# sauvegarde l'image générée par l'application
# éxécuté par le bouton "export"
def save_image():
    # dernière image déssinée
    # last_drawn = (panel, buffer.copy(), resolution, ratio)
    global last_draw
    img = PIL.Image.frombytes(data=bytes(last_draw[1]), mode='RGB', size=last_draw[2])
    img.save('export_%dx%d.png' % last_draw[2])


# ajoute les menus pour controller l'application
def setup_gui(window):
    global settings  # paramètres utilisés pour afficher le résultat

    frame = tk.LabelFrame(window, text="Controls", padx=20, pady=20)
    frame.pack(side=tk.RIGHT)

    # region Resolution

    resolution_frame = tk.LabelFrame(frame, text="resolution", padx=10, pady=10)
    resolution_frame.pack(anchor=tk.NW)
    scale_height = tk.Scale(resolution_frame, label="Height", orient=tk.VERTICAL,
                            length=200, from_=RESOLUTION_MIN_HEIGHT, to=RESOLUTION_MAX_HEIGHT)
    scale_height.set(500)  # default height
    scale_height.pack(side=tk.LEFT, anchor=tk.NW)
    scale_width = tk.Scale(resolution_frame, label="Width", orient=tk.HORIZONTAL,
                           length=200, from_=RESOLUTION_MIN_WIDTH, to=RESOLUTION_MAX_WIDTH)
    scale_width.set(500)  # default width
    scale_width.pack(side=tk.TOP, anchor=tk.NW)

    # endregion Resolution

    # region Render Settings

    render_frame = tk.LabelFrame(frame, text="Render Settings", height=300, padx=10, pady=10)
    render_frame.pack(anchor=tk.NW, fill="x")

    reflection_button = tk.Checkbutton(render_frame, text="Enable Reflections")
    reflection_button.pack(side=tk.TOP, anchor='w')
    transparency_button = tk.Checkbutton(render_frame, text="Enable Transparency")
    transparency_button.pack(side=tk.TOP, anchor='w')
    shadow_button = tk.Checkbutton(render_frame, text="Enable Shadows")
    shadow_button.pack(side=tk.TOP, anchor='w')

    label = tk.Label(render_frame, text="Number of bounces:")
    label.pack(side=tk.TOP, anchor='w', pady=10)

    default = tk.StringVar()
    default.set("5")
    bounce_select = tk.Spinbox(render_frame, from_=1, to=10, width=2, textvariable=default)
    bounce_select.pack(side=tk.TOP, anchor='w')

    # endregion

    buttons = tk.Frame(frame)
    buttons.pack()
    export = tk.Button(buttons, text="Export", command=save_image)
    export.pack(side=tk.LEFT)
    button = tk.Button(buttons, text="Render", command=render)
    button.pack(side=tk.LEFT)

    # menu mis dans une variable globale pour y accéder facilement
    settings = [scale_width, scale_height, reflection_button, transparency_button, shadow_button, bounce_select]


# base de l'application
#
def create_window(render_fn):
    global canvas
    global render_function
    global last_draw

    render_function = render_fn

    window = tk.Tk()
    window.geometry('1620x920')

    def on_delete():
        window.closed = True
        window.destroy()

    window.closed = False
    window.protocol("WM_DELETE_WINDOW", on_delete)

    frame = tk.Frame(window)
    frame.pack(side=tk.LEFT, fill='both', expand=True)
    canvas = tk.Canvas(frame)

    canvas.pack(fill='both', expand=True)

    panel = canvas.create_image(0, 0, image=None, anchor=tk.NW)

    last_draw = (panel, [], (0,0), 0)

    return window, frame, canvas, panel


def draw_image(panel, buffer, resolution=(100, 100)):
    global photo  # else photo isn't drawn
    global last_draw # save last buffer if needed by other functions, (panel, size, buffer)

    win_width = canvas.winfo_width() / resolution[0]
    win_height = canvas.winfo_height() / resolution[1]
    ratio = min(win_height, win_width)
    last_draw = (panel, buffer.copy(), resolution, ratio)
    img = PIL.Image.frombytes(data=bytes(buffer), mode='RGB', size=resolution)
    img = img.resize((int(resolution[0]*ratio), int(resolution[1]*ratio)), 0)

    photo = PIL.ImageTk.PhotoImage(image=img)

    canvas.itemconfig(panel, image=photo)
