# camera :
# [ transform: mat4, fov: float, resolution: [ x: float, y: float ] ]

from maths import vec3, mat4
from math import tan


# Génére un rayon pour chaque pixel de la caméra
def generate_ray(camera):
    orig = mat4.get_pos(camera[0])
    z = camera[2][0] / tan(camera[1]) # Distance of the projection plane from the camera
    
    return tuple(
        (orig, mat4.mul_rot_vec3(camera[0], vec3.normalize((x, y, z))))
        for y in range(camera[2][1]//2, (-camera[2][1])//2, -1)
        for x in range(camera[2][0]//2, (-camera[2][0])//2, -1)
    )