# hittable:
# [ type: int, hittable_properties: list ]

# hittable types:

LIST          = 0
SPHERE        = 1
BOX           = 2
PLANE         = 3
TRIANGLE      = 4
RECT          = 5


# hit_record:
# [ point: vec3, distance: float, normal: vec3, u: float, v: float, front_face: bool, mat: material ]


from math import inf, sqrt
from maths import vec3

from hittables import (
    list as hittables_list,
    sphere,
    box,
    plane,
    triangle,
    rect
)

hittables = (
    hittables_list,
    sphere,
    box,
    plane,
    triangle,
    rect
)


def hit(hittable, ray, d_min, d_max):
    return hittables[hittable[0]].hit(hittable[1], ray, d_min, d_max)


# Detect si le rayon à touché l'intérieur du l'objet
def get_face_normal(ray, outward_normal):
    front_face = vec3.dot(ray[1], outward_normal) < 0

    return front_face, outward_normal if front_face else vec3.neg(outward_normal)