# light:
# [ position: vec3, color: vec3 ]

from math import sqrt
from maths import vec3
from render import ray, hittable, material


# Retourne la couleur de la lumière reçue par le point d'intersection
def get_lighted_color(lights, hit_record, world):
    color = [0, 0, 0]
    for light in lights:
        dist, transparency = check_lighted(light, hit_record, world)
        if dist and transparency:
            vec3.addeq(color, vec3.div_n(vec3.mul(light[1], transparency), dist))
        elif dist:
            vec3.addeq(color, vec3.div_n(light[1], dist))
    
    return color

# Retourne la distance avec la source de lumière si il n'y a aucun objet entre la lumière et le point d'intersection, sinon retourne None
def check_lighted(light, hit_record, world):
    dir = vec3.sub(light[0], hit_record[0])

    if vec3.dot(dir, hit_record[2]) < 0:
        return None, None

    distance = vec3.length_sq(dir)
    vec3.normalize_eq(dir)

    r = ray.from_intersect_point_dir(hit_record[0], dir)

    hit = hittable.hit(world, r, 0, sqrt(distance))
    if not hit:
        return distance, None
    else:
        if (transparency := material.transparency(hit[6], hit)) is None:
            return None, None
        else:
            return distance, vec3.mul_n(transparency, .7)
