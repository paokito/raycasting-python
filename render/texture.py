# texture :
# [ type: int, type_specific_parameters ]


COLOR = 0
CHECKER = 1
LINES = 2
BITMAP = 3
FUNCTION = 4
CHECKER_3D = 5


# COLOR :
# rgb_color: [ r: float, g: float, b: float ]
# CHECKER :
# [ tex0: texture, tex1: texture, length: float, width: float ]
# LINES :
# [ tex0: texture, width0: float, tex1: texture, width1: float ]
# BITMAP :
# bitmap: [ list[color: [r: float, g: float, b: float]], width: int, height: int ]
# FUNCTION :
# fn: function
# CHECKER_3D :
# [ tex0: texture, tex1: texture ]

from math import sin


# Retourne la couleur rgb correspondant au coordonnées (u;v) et aux point p, pour la texture
def value(texture, u, v, p):
    if texture[0] == COLOR:
        return texture[1]
    if texture[0] == CHECKER:
        return value(texture[1][0] if int(u / texture[1][2]) & 1 == int(v / texture[1][3]) & 1 else texture[1][1], u, v, p)
    if texture[0] == LINES:
        return value(texture[1][0] if (u % (texture[1][1] + texture[1][3])) < texture[1][1] else texture[1][2], u, v, p)
    if texture[0] == BITMAP:
        return texture[1][0][min(max(0, int(u * texture[1])), texture[1]-1) + min(max(0, int(v * texture[2])), texture[2]-1) * texture[1]]
    if texture[0] == FUNCTION:
        return texture[1](u, v)
    if texture[0] == CHECKER_3D:
        sines = sin(10*p[0]) * sin(10*p[1]) * sin(10*p[2]);
        return value(texture[1][0] if sines < 0 else texture[1][1], u, v, p)
    raise Exception("Invalid texture type : %d" % texture[0])