# ray :
# [ orig: vec3, dir: vec3 ]

# ray_hit:
# [ point: vec3, dist: float, obj: object, surface_normal: vec3, uv: [x: float, y: float] ]

from math import tan, ceil, inf
from maths import vec3, mat4
from render import texture, material, hittable, light


# Retourne la couleur correspondant au rayon
def get_color(ray, world, lights, background, depth):
    if not depth: return background

    rec = hittable.hit(world, ray, 0, 1000) # Est ce que le rayon touche un objet

    if rec is None:                         # Si non, retourner la couleur du fond
        return background
    
    scatter_rec = material.scatter(rec[6], ray, rec)    # Récupère les rayons réfléchis et réfractés
    color = material.emitted(rec[6], rec)               # Couleur émise par la surface

    if scatter_rec is not None:
        for scattered, attenuation in scatter_rec:
            if scattered is not None:
                vec3.addeq(color, vec3.mul(get_color(scattered, world, lights, background, depth-1), attenuation))
                continue
                
            vec3.addeq(color, vec3.mul(light.get_lighted_color(lights, rec, world), attenuation))   # Si la surface est opaque on l'illumine
    
    return color

# Crée un rayon qui part d'origine p avec une direction dir, en l'éloignant un peu du point p pour qu'il évite d'intersecter la surface de laquelle il part
def from_intersect_point_dir(p, dir):
    return vec3.add(p, vec3.mul_n(dir, 1e-8)), dir