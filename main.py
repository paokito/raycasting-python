from math import sqrt
from time import monotonic as mn
import multiprocessing

from maths import mat4
from render import camera, ray, window
import examples

world, lights, background = examples.cornell_box()


q = multiprocessing.Queue()


CHUNK_SIZE = 10000
NB_THREADS = 10


# Retourne la couleur d'une partie des rayons générés par la camera
def get_colors(settings):
    rs, depth, _p, _i = settings

    buffer = [0]*(len(rs)*3)

    for i, r in enumerate(rs):
        color = ray.get_color(r, world, lights, background, depth)
        for j, c in enumerate(color):
            buffer[i*3+j] = min(max(0, int(sqrt(c)*256)), 255)

    q.put((_i, _p, buffer))
    return buffer


# Fonction appelée lorsque l'utilisateur appuie sur le bouton render
def render_function(panel, w, h, depth):
    # Création de la caméra
    cam = (
        mat4.from_pos_yxzrot_scale((0, 0, 0), (0, 0, 0), (1, 1, 1)),
        45,
        (w, h)
    )

    # Création des rayons, et répartition des rayons en petits paquets qui seront chacun envoyé a un thread
    t = mn()
    ray = camera.generate_ray(cam)
    rays = [[]]
    i = 0
    for x in ray:
        if i == CHUNK_SIZE:
            rays.append([])
            i = 0
        rays[-1].append(x)
        i += 1

    print(len(rays), mn()-t)
    t = mn()

    # Les rayons sont distribués entre plusieurs threads pour améliorer les performances, sans ça le programme est trop lent
    with multiprocessing.Pool(NB_THREADS) as p:
        p.map(get_colors, [(r, depth, (panel, (w, h)), rays.index(r)) for r in rays]) # Chaque thread appelle la fonction get_colors
        p.close()
        p.join()
    
    print(mn() - t)


win, frame, canvas, panel = window.create_window(render_function)
window.setup_gui(win)

# Un remplacement pour window.mainloop() parce qu'on veut pouvoir faire des choses alors que mainloop ne le permet pas
# On récupérer le résultat de chaque thread et afficher l'image petit à petit
i = 0
b = []
while not win.closed:
    if not q.empty():
        a = q.get()

        if len(b) != a[1][1][0] * a[1][1][1] * 3:
            b = [0, 0, 0] * a[1][1][0] * a[1][1][1]
            print(len(b), len(a[2]), a[0])

        b[a[0]*CHUNK_SIZE*3:a[0]*CHUNK_SIZE*3+len(a[2])] = a[2]
        window.draw_image(a[1][0], b, a[1][1])

    win.update()
