# box:
# [ p0: vec3, p1: vec3, inversed_normal: bool, m0: material, m1: material, m2: material, m3: material, m4: material, m5: material ]

from hittables import rect


def hit(box, ray, d_min, d_max):
    for axis in range(3):
        if ray[1][axis] == 0:
            continue

        uI = (axis+1)%3
        vI = (axis+2)%3

        pos = ray[1][axis] > 0
        if (rec := rect.hit((axis, (box[0][uI], box[0][vI]), (box[1][uI], box[1][vI]), (min if pos else max)(box[0][axis], box[1][axis]), -1 if pos^box[2] else 1, box[3+axis*2+pos]), ray, d_min, d_max)) is not None:
            return rec
    
    return None