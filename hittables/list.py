# list:
# list[hittable]

from render import hittable


def hit(list, ray, d_min, d_max):
    closest = d_max
    rec = None

    for target in list:
        tmp_rec = hittable.hit(target, ray, d_min, closest)
        if tmp_rec is not None:
            closest = tmp_rec[1]
            rec = tmp_rec
    
    return rec