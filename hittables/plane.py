# plane:
# [ normal_axis: int, w: float, heading: int, mat: material ]

from maths import vec3
from render import hittable


def hit(plane, ray, d_min, d_max):
    uI = (plane[0]+1)%3
    vI = (plane[0]+2)%3
    wI = plane[0]

    if not ray[1][wI]:
        return None

    d = (plane[1]-ray[0][wI]) / ray[1][wI]
    if d < d_min or d > d_max:
        return None

    u = ray[0][uI] + d*ray[1][uI]
    v = ray[0][vI] + d*ray[1][vI]

    p = vec3.add(ray[0], vec3.mul_n(ray[1], d))
    n = [0, 0, 0]
    n[wI] = plane[2]
    front_face, n = hittable.get_face_normal(ray, n)
    return (
        p,
        d,
        n,
        u%1,
        v%1,
        front_face,
        plane[3]
    )