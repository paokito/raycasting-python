# sphere:
# [ center: vec3, radius: float, mat: material ]

from math import sqrt, atan2, acos, pi
from maths import vec3
from render import hittable


def hit(sphere, ray, d_min, d_max):
    oc = vec3.sub(ray[0], sphere[0])                    # oc = ray.orig - sphere.center
    dot = vec3.dot(ray[1], oc)                          # dot = ray.dir . oc
    delta = dot*dot - (vec3.length(oc)**2-sphere[1]**2) # delta = dot^2 - (||oc||^2 - sphere.radius^2)
    if delta < 0:
        return None

    s = sqrt(delta)
    dist = -dot - s
    
    if not (d_min < dist < d_max):
        dist = -dot + s
        if not (d_min < dist < d_max):
            return None

    p = vec3.add(ray[0], vec3.mul_n(ray[1], dist))    # point = ray.orig + ray.dir * tmp
    n = vec3.div_n(vec3.sub(p, sphere[0]), sphere[1]) # (point - sphere.center) / sphere.radius
    front_face, n = hittable.get_face_normal(ray, n)
    return (
        p,
        dist,
        n,
        (atan2(-n[2], n[0]) + pi) / (2*pi),
        (acos(-n[1])) / pi,
        front_face,
        sphere[2]
    )