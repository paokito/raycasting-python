# rect:
# [ normal_axis: int, p0: vec2, p1: vec2, w: float, heading: int, mat: material ]

from maths import vec3
from render import hittable

def hit(rect, ray, d_min, d_max):
    uI = (rect[0]+1)%3
    vI = (rect[0]+2)%3
    wI = rect[0]

    if not ray[1][wI]:
        return None

    d = (rect[3]-ray[0][wI]) / ray[1][wI]
    if d < d_min or d > d_max:
        return None

    u = ray[0][uI] + d*ray[1][uI]
    v = ray[0][vI] + d*ray[1][vI]
    if u < rect[1][0] or u > rect[2][0] or v < rect[1][1] or v > rect[2][1]:
        return None

    p = vec3.add(ray[0], vec3.mul_n(ray[1], d))
    n = [0, 0, 0]
    n[wI] = rect[4]
    front_face, n = hittable.get_face_normal(ray, n)
    return (
        p,
        d,
        n,
        (u-rect[1][0])/(rect[2][0]-rect[1][0]),
        (v-rect[1][1])/(rect[2][1]-rect[1][1]),
        front_face,
        rect[5]
    )