# triangle:
# [ p0: vec3, p1: vec3, p2: vec3, normal: vec3, mat: material ]

from maths import vec3
from render import hittable


def hit(triangle, ray, d_min, d_max):
    edge1 = vec3.sub(triangle[1], triangle[0])
    edge2 = vec3.sub(triangle[2], triangle[0])

    pvec = vec3.cross(ray[1], edge2)
    det = vec3.dot(pvec, edge1)

    if -1e-15 < det < 1e-15: return None

    invdet = 1/det

    tvec = vec3.sub(ray[0], triangle[0])

    u = vec3.dot(pvec, tvec) * invdet
    if u < 0 or u > 1: return None 

    qvec = vec3.cross(tvec, edge1)
    v = vec3.dot(qvec, ray[1]) * invdet

    if v < 0 or u + v > 1.: return None

    t = vec3.dot(qvec, edge2) * invdet

    if t < d_min or t > d_max: return None

    p = vec3.add(ray[0], vec3.mul_n(ray[1], t))
    front_face, n = hittable.get_face_normal(ray, triangle[3])
    return (
        p,
        t,
        n,
        u,
        v,
        front_face,
        triangle[4]
    )